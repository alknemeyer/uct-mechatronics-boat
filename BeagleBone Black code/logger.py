# Author:           Alexander Knemeyer
# Created:			February 2017
# Email:            alknemeyer@gmail.com

import GPS_lib
import IMU_lib
import time
import os.path

# THE PROCESS I FOLLOWED TO MAKE THIS SCRIPT RUN AT STARTUP:
# create a launcher.sh script in /root directory
# this is a set of shell instructions which run the python script
# make it executable using "chmod 755 launcher.sh" while in /root directory
# create/modify a cronjob to launch this script

# -> cronjob is a daemon which runs commands at specified times, on reboot, etc
# COMMAND					DESCRIPTION
# crontab -l				view existing cronjobs
# crontab -e				edit/comment out the cronjob
# pkill -f logger.py		stop the script from running in the background
# pgrep -af python			see a list of running python scripts

# TO STOP THE SCRIPT FROM RUNNING AT STARTUP:
# > crontab -e
# comment out the line "@reboot /root/launcher.sh" with a #
# exit by pressing "ctrl+X", then "y", then "enter"

# FOR A COOL WAY OF VISUALISING THINGS:
# check out http://www.gpsvisualizer.com/map_input?form=googleearth

class Logger:
	def __init__(self, use_GPS=True, use_IMU=True):
		self.use_GPS = use_GPS
		self.use_IMU = use_IMU
		
		self.filename = ''
		
		if use_IMU:
			print('Initialising IMU')
			self.imu = IMU_lib.IMU('UART2', calibrate_gyro=False)
			self.imu.read()
			self.filename += 'IMU_'
		
		if use_GPS:
			print('Initialising GPS')
			self.gps = GPS_lib.GPS('UART1')
			self.gps.read()
			self.filename += 'GPS_' + self.gps.timeUTC
			
		# check that there isn't already a file with the same name (avoid overwriting things)
		number = 0
		while os.path.isfile('logs/' + self.filename + str(number) + '.csv'):
			number += 1
		
		self.filename += str(number) +  '.csv'
		
		# create file and write headers
		f = open('logs/' + self.filename, 'wb')
		
		if use_GPS:
			f.write('timeUTC,sats,latitude,longitude,knots,altitude,')
		
		if use_IMU:
			f.write('acc_x,acc_y,acc_z,mag_x,mag_y,mag_z,gyro_x,gyro_y,gyro_z,gyro_angle_x,gyro_angle_y,gyro_angle_z,temperature')
			
		f.write('\n')
		f.close()

	def log(self):
		# write the data
		f = open('logs/' + self.filename, 'a')
		
		if self.use_GPS:
			self.gps.read()
			f.write(self.gps.timeUTC+','+self.gps.sats+','+str(self.gps.latDec)+','+str(self.gps.lonDec)+','+self.gps.knots+','+self.gps.altitude + ',')
		
		if self.use_IMU:
			self.imu.read()
			f.write(  str(self.imu.acc[0]) + ',' + str(self.imu.acc[1]) + ',' + str(self.imu.acc[2]) + ','
					+ str(self.imu.mag[0]) + ',' + str(self.imu.mag[1]) + ',' + str(self.imu.mag[2]) + ','
					+ str(self.imu.gyro[0]) + ',' + str(self.imu.gyro[1]) + ',' + str(self.imu.gyro[2]) + ','
					+ str(self.imu.gyro_angle[0]) + ',' + str(self.imu.gyro_angle[1]) + ',' + str(self.imu.gyro_angle[2]) + ','
					+ str(self.imu.temperature))
		
		f.write('\n')
		f.close()

# use a try... except approach in case an error happens and the program needs to handle it gracefully
# might interfere with troubleshooting - consider commenting out the "try:" and every after "time.sleep(1.0 - elapsed_time) for easier bug-tracking
try:
	logger = Logger(use_GPS=False)
	
	print 'filename:', logger.filename, '\n'
	
	# the loop
	while True:
		start_time = time.time()				# get the time
		
		# log the data to a csv file
		logger.log()
		print('Added row of data')
		
		elapsed_time = time.time() - start_time
		
		time.sleep(1.0 - elapsed_time)

except KeyboardInterrupt:
	pass

except Exception as e:
	print e
	print('Something went wrong')
  
finally:
	print '\nThe data has been saved in var/lib/cloud9/scripts/logs/'+ logger.filename
