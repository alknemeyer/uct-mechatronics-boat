# Author:           Alexander Knemeyer
# Created:  		23 February 2017
# Email:            alknemeyer@gmail.com

import serial
import Adafruit_BBIO.UART as UART
from time import sleep

UART_num = '4'

UART.setup('UART' + UART_num)
ard = serial.Serial('/dev/ttyO' + UART_num, 57600)

while 1:
    x = raw_input()
    ard.write(x)
