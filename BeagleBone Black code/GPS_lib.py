# Author:           Alexander Knemeyer
# Created:  		February 2017
# Email:            alknemeyer@gmail.com

import serial
import Adafruit_BBIO.UART as UART
from time import sleep

# sets the GPS to communicate at baud rate of 57600, and sets it to
# read and report at 5 Hz
# For reference: Menzies lab is 33.958900 S, 18.460044 E
# Note that the GPS takes a long time to get a signal, and doesn't work at all
# indoors

class GPS:
	def __init__(self, UART_num):
		""" Initialises GPS object and configures GPS module """
		# start connection with GPS
		UART.setup(UART_num)
		self.ser = serial.Serial('/dev/ttyO' + UART_num[4], 9600)
		
		##### Set up variables for useful commands:
		# The numbers after the star is a check to ensure the data arrived correctly
		# This set is used to set the rate the GPS reports
		UPDATE_10_sec =   "$PMTK220,10000*2F\r\n"   # Update Every 10 Seconds
		UPDATE_5_sec =    "$PMTK220,5000*1B\r\n"    # Update Every 5 Seconds  
		UPDATE_1_sec =    "$PMTK220,1000*1F\r\n"    # Update Every One Second
		UPDATE_200_msec = "$PMTK220,200*2C\r\n"     # Update Every 200 Milliseconds
		
		# This set is used to set the rate the GPS takes measurements
		MEAS_10_sec =  "$PMTK300,10000,0,0,0,0*2C\r\n"  # Measure every 10 seconds
		MEAS_5_sec =   "$PMTK300,5000,0,0,0,0*18\r\n"   # Measure every 5 seconds
		MEAS_1_sec =   "$PMTK300,1000,0,0,0,0*1C\r\n"   # Measure once a second
		MEAS_200_msec= "$PMTK300,200,0,0,0,0*2F\r\n"    # Meaure 5 times a second
		
		# Set the Baud Rate of GPS
		BAUD_57600 = "$PMTK251,57600*2C\r\n"            # Set Baud Rate to 57600
		BAUD_9600 =  "$PMTK251,9600*17\r\n"             # Set Baud Rate to 9600
		
		# Commands for which NMEA Sentences are sent
		GPRMC_ONLY =   "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"      # Send only the GPRMC Sentence
		GPRMC_GPGGA =  "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"      # Send GPRMC AND GPGGA Sentences
		SEND_ALL =     "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"      # Send All Sentences
		SEND_NOTHING = "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"      # Send Nothing
		
		# Actually configuring the GPS now
		self.ser.write(BAUD_57600)      # Set Baud Rate to 57600
		sleep(1)                        # Pause
		self.ser.baudrate = 57600       # IMPORTANT! Change serial baudrate to match GPS
		
		self.ser.write(UPDATE_200_msec)		# Set update rate
		sleep(0.5)
		
		self.ser.write(MEAS_200_msec)		# Set measurement rate
		sleep(0.5)
		
		self.ser.write(GPRMC_GPGGA)			# Ask for only GPRMC and GPGGA Sentences
		sleep(0.5)
		
		# initialise GPS fix variable to zero
		self.fix = 0
		
		print("GPS has been initialised")
		
	def read(self):
		""" Update GPS object's member variables """
		# flush input buffer to insure we're not getting old data
		self.ser.flushInput()
		# clear input buffer a second time in case the buffer gets
		# refilled while it's getting flushed (resulting in a partial fill)
		self.ser.flushInput()
		
		# create NMEA1, NMEA2 and NMEA3 - they will be assigned the strings GPRMC
		# and GPGGA, each of which gives us slightly different info
		
		while self.ser.inWaiting() == 0:
		    pass
		self.NMEA1 = self.ser.readline()
		
		while self.ser.inWaiting() == 0:
		    pass
		self.NMEA2 = self.ser.readline()
		
		while self.ser.inWaiting() == 0:
		    pass
		self.NMEA3 = self.ser.readline()
		
		NMEA1_array = self.NMEA1.split(',')
		NMEA2_array = self.NMEA2.split(',')
		NMEA3_array = self.NMEA3.split(',')
		
		# GPRMC, GPGGA, etc are strings from the GPS which hold different kinds
		# of useful GPS data
		if NMEA1_array[0] == '$GPRMC':
			self.timeUTC = NMEA1_array[1][:-8] + ':' + NMEA1_array[1][-8:-6] + ':' + NMEA1_array[1][-6:-4]
			self.latDeg = NMEA1_array[3][:-7]
			self.latMin = NMEA1_array[3][-7:]
			self.latHem = NMEA1_array[4]
			self.lonDeg = NMEA1_array[5][:-7]
			self.lonMin = NMEA1_array[5][-7:]
			self.lonHem = NMEA1_array[6]
			self.knots = NMEA1_array[7]
		    
		if NMEA1_array[0] == '$GPGGA':
			self.fix = NMEA1_array[6]
			self.altitude = NMEA1_array[9]
			self.sats = NMEA1_array[7]
    
		if NMEA2_array[0] == '$GPRMC':
			self.timeUTC = NMEA2_array[1][:-8] + ':' + NMEA2_array[1][-8:-6] + ':' + NMEA2_array[1][-6:-4]
			self.latDeg = NMEA2_array[3][:-7]
			self.latMin = NMEA2_array[3][-7:]
			self.latHem = NMEA2_array[4]
			self.lonDeg = NMEA2_array[5][:-7]
			self.lonMin = NMEA2_array[5][-7:]
			self.lonHem = NMEA2_array[6]
			self.knots = NMEA2_array[7]
		
		if NMEA2_array[0] == '$GPGGA':
			self.fix = NMEA2_array[6]
			self.altitude = NMEA2_array[9]
			self.sats = NMEA2_array[7]
		    
		if NMEA3_array[0] == '$GPRMC':
			self.timeUTC = NMEA3_array[1][:-8] + ':' + NMEA3_array[1][-8:-6] + ':' + NMEA3_array[1][-6:-4]
			self.latDeg = NMEA3_array[3][:-7]
			self.latMin = NMEA3_array[3][-7:]
			self.latHem = NMEA3_array[4]
			self.lonDeg = NMEA3_array[5][:-7]
			self.lonMin = NMEA3_array[5][-7:]
			self.lonHem = NMEA3_array[6]
			self.knots = NMEA3_array[7]

		if NMEA3_array[0] == '$GPGGA':
			self.fix = NMEA3_array[6]
			self.altitude = NMEA3_array[9]
			self.sats = NMEA3_array[7]
		
		# format the longitude and latitude nicely, and sotre in latDec and lonDec
		if self.latDeg != '' and self.lonDeg != '': 
			self.latDec = float(self.latDeg) + float(self.latMin)/60. 
			self.lonDec = float(self.lonDeg) + float(self.lonMin)/60. 
			if gps.lonHem == 'W':
				self.lonDec = (-1) * self.lonDec
			if gps.latHem == 'S':
				self.latDec = (-1) * self.latDec
		else:
			self.latDec, self.lonDec = 0, 0

	def debug(self):
		""" Continuously outputs (possibly) helpful data - useful for debugging """
		gps = GPS("UART1")
		while 1:
			gps.read()
			print('----------------------------------------------')
			print("NMEA1:", gps.NMEA1)
			print("NMEA2:", gps.NMEA2)
			print('NMEA3:', gps.NMEA3)
			
			if gps.fix != 0 and gps.sats > 0:
				print('Universal Time:', gps.timeUTC)
				print('Tracking:', gps.sats, 'satellites')
				print('Latitude:', gps.latDeg, 'degrees', gps.latMin,'minutes', gps.latHem, '==', gps.latDec)
				print('Longitude:', gps.lonDeg, 'degrees', gps.lonMin,'minutes', gps.lonHem, '==', gps.lonDec)
				print('Speed:', gps.knots)
				print('Altitude:', gps.altitude)

# gps = GPS(UART_num='UART1')
# gps.read()
# gps.debug()
