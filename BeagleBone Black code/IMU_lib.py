# Author:           Alexander Knemeyer
# Created:  		22 February 2017
# Email:            alknemeyer@gmail.com

import serial
import Adafruit_BBIO.UART as UART
import time
from ctypes import *

def convert_to_float(data_in):
	""" Converts an array of raw binary data to float
		Made this because python really messes with data types.
		Might be unnecessarily complicated - need to work on python
		data handling skills """
	# remove the '\x' and convert to string so python doesnt try interpret the numbers as special characters
	# look up 'list comprehensions' if confusing
	arr = [str(hex(ord(i)))[2:] for i in data_in]
	
	s = ''
	
	for i in arr:
		if len(i) == 1:				# python removes leading '0's - put em back in
			s += '0' + i
		else:
			s += i

	i = int(s, 16)                   # convert from hex to a Python int
	cp = pointer(c_int(i))           # make this into a c integer
	fp = cast(cp, POINTER(c_float))  # cast the int pointer to a float pointer
	return fp.contents.value         # dereference the pointer, get the float

class IMU:
	def __init__(self, UART_num, calibrate_gyro=False):
		""" Initialises serial communication with IMU, member variables and optionally
			gyro calibration """
		# start connection with INEMO-M1
		UART.setup(UART_num)
		self.ser = serial.Serial('/dev/ttyO' + UART_num[4], 57600)
		
		# variables
		self.acc =  [0, 0, 0]
		self.mag =  [0, 0, 0]
		self.gyro = [0, 0, 0]
		self.gyro_offset = [0, 0, 0]
		self.use_gyro_offset = False
		self.gyro_angle = [0, 0, 0]
		self.temperature = 0
		self.time_at_last_read = time.time()		# used to calculate the gyro's actual angle (not angular velocity)
		
		# if calibrate_gyro:
		# 	print('Calibrating gyro. If nothing happens, the BBB probably isn\'t receiving data from the IMU')
		# 	self.calibrate_gyro()
		
		print('IMU initialised')
		
	def read(self):
		""" Reads sensors (acc, mag, gyro and temperature) and updates attributes
			of IMU object """
		
		# self.update_gyro_angle()			# this has to be done before updating the gyro angle - see function description
		
		# flush input buffer to insure we're not getting old data
		self.ser.flushInput()
		# clear input buffer a second time in case the buffer gets
		# refilled while it's getting flushed (resulting in a partial fill)
		self.ser.flushInput()
		
		# wait for serial data
		while self.ser.inWaiting() == 0:
			pass
		
		IMU_data = ord(self.ser.read())
		while IMU_data != 254:						# The number 254 signals the start of the data
			IMU_data = ord(self.ser.read())
			
		# at this point, the next byte read in should be acc[0][0]
		
		for sensor in [self.acc, self.mag, self.gyro, self.gyro_angle]:
			for axis in range(3):
				# read in four bytes
				temp = [self.ser.read(), self.ser.read(), self.ser.read(), self.ser.read()]
				
				# seem to have to flip the order for some reason
				# took me way too long to realise
				v = [temp[3], temp[2], temp[1], temp[0]]
				
				sensor[axis] = convert_to_float(v)
				
		# temperature is negative coefficient
		self.temperature = 45 - ord(self.ser.read())
		
		end = ord(self.ser.read()) 
		
		# the IMU sends the number 254, then 49 bytes of data, and finally the
		# number 251 as a check that all the data came through alright
		if end != 251:
			print('Something went wrong - some sensor data may have been missed')
			return
		
		# if the gyro has been calibrated, use the offset
		# if self.use_gyro_offset:
		# 	for i in range(3):
		# 		self.gyro[i] -= self.gyro_offset[i]
		
	def calibrate_gyro(self):
		""" takes 25 readings for gyro x, y and z, calculates an average for each
			and uses it as an offset.
			
			I made this before realising that timing becomes easier if the IMU
			handles the calculation of the gyro's angle. Obsolete now, but left
			here just in case.
			"""
		self.gyro_offset = [0.0, 0.0, 0.0]
		
		num_readings = 25
		
		for i in range(num_readings):
			self.read()
			self.gyro_offset[0] += self.gyro[0]
			self.gyro_offset[1] += self.gyro[1]
			self.gyro_offset[2] += self.gyro[2]
		
		self.gyro_offset[0] = self.gyro_offset[0] / num_readings		# take mean of readings
		self.gyro_offset[1] = self.gyro_offset[1] / num_readings
		self.gyro_offset[2] = self.gyro_offset[2] / num_readings
		
		self.use_gyro_offset = True
		
		print('Gyro has been calibrated')
	
	def update_gyro_angle(self):
		""" Get the previous angular velocity and multiply it by the time spent
			at that velocity. Formula:
			angle = (angular velocity) * (change in time)
			
			I made this before realising that timing becomes easier if the IMU
			handles the calculation of the gyro's angle. Obsolete now, but left
			here just in case."""
		
		self.delta_t = self.time_at_last_read - time.time()
		for i in range(3):
			self.gyro_angle[i] += self.gyro[i] * self.delta_t
		
		self.time_at_last_read = time.time()
	
	def debugging(self):
		""" Contiuously outputs sensor values. Useful for debugging """
		while 1:
			imu.read(UART_num='UART2', calibrate_gyro=False)
			print '========================================='
			print '\tx\ty\tz\tunits'
			print 'acc\t', round(imu.acc[0],2), '\t', round(imu.acc[1],2), '\t', round(imu.acc[2],2), '\t', '+- 4g'
			print 'mag\t', round(imu.mag[0],2), '\t', round(imu.mag[1],2), '\t', round(imu.mag[2],2), '\t', 'gauss'
			print 'gyro\t', round(imu.gyro[0],2), '\t', round(imu.gyro[1],2), '\t', round(imu.gyro[2],2), '\t', 'deg per second'
			print 'gyroDeg\t', round(imu.gyro_angle[0],2), '\t', round(imu.gyro_angle[1],2), '\t', round(imu.gyro_angle[2],2), '\t', 'deg'
			print 'temp\t', imu.temperature, '\t', 'deg C (might be incorrect)'
			time.sleep(0.5)

# imu = IMU(UART_num='UART2', calibrate_gyro=False)
# imu.calibrate_gyro()
# imu.debugging()
