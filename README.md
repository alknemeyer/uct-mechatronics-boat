# README #

This repository contains the information needed to get an easy start with the UCT mechatronics boat platform.
Version:	1.0

### Contribution guidelines ###

Only commit changes which make sense to have on the boat as a general platform - not things which are only useful for your specific research/project. Add useful code, guides, etc in a separate folder.

### Who do I talk to? ###

Robyn Verrinder runs the boat platform
Alexander Knemeyer set up and owns the repo